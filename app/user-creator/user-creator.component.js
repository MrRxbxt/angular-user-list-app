angular
  .module('userCreator')
  .component('userCreator', {
    templateUrl: 'app/user-creator/user-creator.template.html',
    controller: ['$http', function ($http) {

      this.error = null;
      this.successful = null;

      this.handleSubmit = () => {
        $http.post('http://localhost:3000/users', {
            name: this.userName,
            address: this.userAddress,
            email: this.userEmail,
            age: this.userAge
          })
          .then(res => {
            this.successful = 'Created successfully !'
          })
          .catch(err => {
            this.error = 'Something went wrong when creating the user...'
          })
      }
    }]
  })
angular
  .module('userList')
  .component('userList', {
    templateUrl: 'app/user-list/user-list.template.html',
    controller: ['$http', function($http) {
      this.orderProp = 'name';

      this.error = null;
      this.successful = null;

      this.handleClick = (userId) => {
        $http.delete(`http://localhost:3000/users/${userId}`)
          .then(res => {
            this.users = this.users.filter(user => user._id !== userId)
            this.successful = 'Deleted successfully !'
          })
          .catch(err => {
            this.error = 'Something went wrong when updating the user...'
          })
      }

      $http.get('http://localhost:3000/users')
        .then((res) => {
          this.users = res.data;
        });
    }]
  })
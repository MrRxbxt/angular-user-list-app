angular
  .module('userDetail')
  .component('userDetail', {
    templateUrl: 'app/user-detail/user-detail.template.html',
    controller: ['$routeParams', '$http', function($routeParams, $http) {
      this.error = null;
      this.successful = null;

      this.handleSubmit = (e) => {
        this.isFetching = true;

        $http.put(`http://localhost:3000/users/${$routeParams.userId}`, {
          name: this.userName,
          address: this.userAddress,
          email: this.userEmail,
          age: this.userAge
        })
          .then((res) => {
            this.successful = 'Updated successfully !';
          })
          .catch((err) => {
            this.error = "Some error occurred while updating the user...";
          })
      }
      
      $http.get(`http://localhost:3000/users/${$routeParams.userId}`)
        .then((res) => {
          const user = res.data;

          this.userId = user._id ? `ID: ${(user._id)}` : null;
          this.userName = user.name;
          this.userAddress = user.address;
          this.userEmail = user.email;
          this.userAge = user.age; 
        })
    }]
  })